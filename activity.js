// Insert Documents
db.users.insertMany([
	{
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
        courses: [ "Python", "React", "PHP" ],
        department: "HR"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neilarmstrong@gmail.com"
        },
        courses: [ "React", "Laravel", "Sass" ],
        department: "HR"
    },
    {
    	firstName: "Jane",
    	lastName: "Doe",
    	age: 21,
    	contact: {
    		phone: "87654321",
    		email: "janedoe@gmail.com"
    	},
    	courses: ["CSS", "JavaScript", "Python"],
    	department: "HR"
    },
    {
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "12345678",
			email: "bill@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations",
		status: "active"
	},
]);

// Insert embedded array
db.users.insertOne({
	namearr: [
		{
			namea: "juan"
		},
		{
			nameb: "tamad"
		}
	]
});

// #2 users with s in firstName or d in lastName, show only firstName and lastName fields, hide _id
db.users.find(
	{$or: [
			{firstName: {$regex: "s", $options: "i"}},
			{lastName: {$regex: "d", $options: "i"}}
		]
	},
	{
		firstName: 1,
		lastName: 1,
		_id:0
	}
);

// #3 Users from HR with age greater than or equal to 70
db.users.find({
	$and:
	[
		{department: "HR"},
		{age: {$gte: 70}}
	]
});

// #4 users with e in first name and age less than or equal to 30
db.users.find(
	{ $and:
		[
			{firstName: {$regex: "e", $options: "i"}},
			{age:{$lte: 30}}
		]

	}
);